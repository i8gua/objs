from setuptools import setup

setup(
  name='objs',
  version="0.0.1",
  description="object like js",
  author="zuroc",
  author_email="zsp042@gmail.com",
  packages=['objs'],
  zip_safe=True,
  include_package_data=True,
  install_requires=[],
  url="https://gitee.com/i8gua/objs",
)
